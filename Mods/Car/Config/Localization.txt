Key,Source,Context,Changes,English
pjVehicle4x4TruckWhitePlaceable,blocks,Block,New,"Scrubed 4x4"
pjVehicle4x4TruckBluePlaceable,blocks,Block,New,"4x4, Blue"
pjVehicle4x4TruckRedPlaceable,blocks,Block,New,"4x4, Red"
pjVehicle4x4TruckSpartanPlaceable,blocks,Block,New,"4x4, Spartan Black and Gold"
pjVehicle4x4TruckMissSpartanPlaceable,blocks,Block,New,"4x4, Miss Spartan's Pink Rider"
pjVehicle4x4TruckPatrolPlaceable,blocks,Block,New,"4x4, Patrol Blue"
pjVehicle4x4TruckTestPlaceable,blocks,Block,New,"4x4, UV Map Test Pattern"
pjVehicleMotorcycleTestPlaceable,blocks,Block,New,"Motorcyle, UV Map Test Pattern"
