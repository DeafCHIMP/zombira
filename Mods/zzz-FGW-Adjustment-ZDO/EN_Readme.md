Adjustment of 7D2D-Game (and Server) Settings

Version 20200816

A) Optimize Gunpowder and JunkTurretAmmo recipes
   [recipes.xml]
     Gunpoder:
                     Coal          +1 (vanilla 2 / now 3)
                     NitratePowder +1 (vanilla 2 / now 3)
     JunkTurretAmmo:
                     ScrapIron     +2 (vanilla 3 / now 5)

B) Make DecoTier usable and Wheels craftable
   [recipes.xml] [blocks.xml]

C) For more creepy feeling an light fog will roll in at night
   [worldglobal.xml] (Off until further notice)

D) Some things that can be picked up should be made pickable
   [blocks.xml]
     SANDBAGS / TRAPS / PAINTINGS / PICTURE FRAME / BARBED FENCE / BARBED WIRE /
     FARM PLOTS / MINES / (div. PLANTS deactivated because Zombie Dayz does this work with "KHA19-Pickup Plants")

E) Enable preLoad for Zombiemeshfile
   [items.xml]

F) Spawning Control
   [spawning.xml]

G) Adding a little speed nudge to the cardio rule
   [progression.xml]

H) Max player lvl Control (set to 333 / on vanilla 300)
   [progression.xml]

I) 15% Inrease Headshot Damage
   [entityclasses.xml]

J) Optimize quest reward for the challenges
   [quests.xml]
     EXP   +250 (vanilla 500 / now 750)
     Coins +150 (vanilla 350 / now 500)

K) Make Beaker craftable in your forge with crucible
   [recipes.xml]
     Recipe: 35 Glass / 15 Lead / 15 Clay

L) Trader optimization
   [traders.xml]

M) optimize Harvesting resources
   [blocks.xml]

N) Vanilla Paintings craftable
   [recipes.xml]