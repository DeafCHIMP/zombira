Anpassung der 7D2D-Spiel- (und Server-) Einstellungen

Fassung 20200816

A) Optimieren von Schießpulver- und JunkTurretMunition-Rezepte
   [recipes.xml]
     Schießpulver:
                     Kohle 			+1 (Orginal 2 / jetzt 3)
                     NitratPulver 	+1 (Orginal 2 / jetzt 3)
     JunkTurretAmmo:
                     Alteisen 		+2 (Orginal 3 / jetzt 5)

B) DecoTier nutzbar und Räder herstellbar machen
   [Rezepte.xml] [Blöcke.xml]

C) Auger and chainsaw quiter when ducking
   [sound.xml]

D) Einige Dinge, die aufhebbar sein sollten, werden aufhebbar gemacht
   [Blöcke.xml]
     SANDSÄCKE / FALLEN / GEMÄLDE / BILDERRAHMEN / STACHELDRAHTZAUN / STACHELDRAHT /
     PFLANZKÜBEL / MINEN / (div. PFLANZEN deaktiviert, weil Zombie Dayz diese Arbeit mit "KHA19-Pickup-Pflanzen" macht)

E) Aktivieren von preLoad für Zombiemeshfile
   [items.xml]

F) Laichkontrolle
   [laichen.xml]

G) Hinzufügen eines kleinen Geschwindigkeitsschubs zur Cardio-Regel
   [progression.xml]

H) Max Spieler lvl Kontrolle (auf 333 gesetzt / im Orginal 300)
   [progression.xml]

I) 15% Schadensbonus bei Kopftreffer
   [entityclasses.xml]

J) Optimierung der Questbelohnung für die Herausforderungen die man beim Händler annehmen kann
   [quests.xml]
     EXP +250 (Vanille 500 / jetzt 750)
     Münzen +150 (Vanille 350 / jetzt 500)

K) Machen Sie Beaker in Ihrer Schmiede mit Tiegel handhabbar
   [rezepte.xml]
     Das Rezept: 35 Glas / 15 Blei / 15 Ton

L) Optimierung des Händlers
   [Händler.xml]

M) Optimierung der Ernteressourcen
   [Blöcke.xml]

N) Vanille-Gemälde an der Werkbank herstellbar
   [rezepte.xml]